# Module: Ayrshare

Description
===========
Unofficial Ayrshare integration module.

  Ayrshare is a powerful set of APIs that enable you to send social media posts,
  get analytics, and add comments to Twitter, Instagram, Facebook, LinkedIn,
  YouTube, Google My Business, Pinterest, TikTok, Reddit, and Telegram on behalf
  of your users.

  The Ayrshare API handles all the setup and maintenance for the social media
  networks.

Requirements
============
* An Ayrshare API key (https://app.ayrshare.com/api).

Installation
============
The module can be installed via the standard Drupal installation process
Usage

Usage
=====
Go to the Ayrshare API Form (/admin/config/services/ayrshare-api) and enter
your API Key.

Documentation
=============

* API Overview: https://docs.ayrshare.com/rest-api/overview
* Troubleshooting: https://docs.ayrshare.com/additional-info/troubleshooting
