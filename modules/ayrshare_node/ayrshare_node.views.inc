<?php

/**
 * @file
 * Provide views data for ayrshare_node.module.
 */

/**
 * Implements hook_views_data().
 */
function ayrshare_node_views_data() {
  $data = [];

  // Ayrshare Pos Base data.
  $data['ayrshare_post']['table']['group'] = t('Ayrshare Post');
  $data['ayrshare_post']['table']['base'] = [
    'title' => t('Ayrshare Post'),
    'help' => t('Ayrshare Post data from ayrshare_post DB table.'),
  ];

  // Ayrshare Pos Fields.
  $data['ayrshare_post']['id'] = [
    'title' => t('Entry ID'),
    'help' => t('Entry ID.'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
  ];

  $data['ayrshare_post']['pid'] = [
    'title' => t('Post ID'),
    'help' => t('Post entry ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];

  $data['ayrshare_post']['tid'] = [
    'title' => t('Top Level ID'),
    'help' => t('Top Level entry ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];

  $data['ayrshare_post']['nid'] = [
    'title' => t('Node ID'),
    'help' => t('The node ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'relationship' => [
      'title' => t('Node'),
      'help' => t('The node entity used in the Post.'),
      'base' => 'node',
      'base field' => 'nid',
      'id' => 'standard',
    ],
  ];

  $data['ayrshare_post']['platform'] = [
    'title' => t('Platform'),
    'help' => t('Social platform name.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];

  // Ayrshare Comment Base data.
  $data['ayrshare_comment']['table']['group'] = t('Ayrshare Comment');
  $data['ayrshare_comment']['table']['base'] = [
    'title' => t('Ayrshare Comment'),
    'help' => t('Ayrshare Comment data from ayrshare_comment DB table.'),
  ];

  // Ayrshare Comment Fields.
  $data['ayrshare_comment']['id'] = [
    'title' => t('Entry ID'),
    'help' => t('Entry ID.'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
  ];

  $data['ayrshare_comment']['cid'] = [
    'title' => t('Comment ID'),
    'help' => t('Comment entry ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];

  $data['ayrshare_comment']['tid'] = [
    'title' => t('Top Level ID'),
    'help' => t('Top Level entry ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];

  $data['ayrshare_comment']['nid'] = [
    'title' => t('Node ID'),
    'help' => t('The node ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'relationship' => [
      'title' => t('Node'),
      'help' => t('The node entity used in the Post.'),
      'base' => 'node',
      'base field' => 'nid',
      'id' => 'standard',
    ],
  ];

  $data['ayrshare_comment']['comment'] = [
    'title' => t('Comment'),
    'help' => t('The comment content.'),
    'field' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];

  $data['ayrshare_comment']['from'] = [
    'title' => t('From'),
    'help' => t('The author information.'),
    'field' => [
      'id' => 'ayrshare_unserialized_data',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];

  $data['ayrshare_comment']['created'] = [
    'title' => t('Creation Date Time'),
    'help' => t('The creation date from the comment entry.'),
    'field' => [
      'id' => 'date',
      'click sortable' => TRUE,
    ],
    'filter' => [
      'id' => 'date',
    ],
  ];

  $data['ayrshare_comment']['platform'] = [
    'title' => t('Platform'),
    'help' => t('Social platform name.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];

  // Ayrshare Analytics Base data.
  $data['ayrshare_analytics']['table']['group'] = t('Ayrshare Analytics');
  $data['ayrshare_analytics']['table']['base'] = [
    'title' => t('Ayrshare Analytics'),
    'help' => t('Ayrshare Analytics data from ayrshare_analytics DB table.'),
  ];

  // Ayrshare Analytics Fields.
  $data['ayrshare_analytics']['id'] = [
    'title' => t('Entry ID'),
    'help' => t('Entry ID.'),
    'field' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
  ];

  $data['ayrshare_analytics']['pid'] = [
    'title' => t('Analytics ID'),
    'help' => t('Analytics entry ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];

  $data['ayrshare_analytics']['tid'] = [
    'title' => t('Top Level ID'),
    'help' => t('Top Level entry ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];

  $data['ayrshare_analytics']['nid'] = [
    'title' => t('Node ID'),
    'help' => t('The node ID.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'relationship' => [
      'title' => t('Node'),
      'help' => t('The node entity used in the Analytics.'),
      'base' => 'node',
      'base field' => 'nid',
      'id' => 'standard',
    ],
  ];

  $data['ayrshare_analytics']['data'] = [
    'title' => t('Data'),
    'help' => t('The analytics data information.'),
    'field' => [
      'id' => 'ayrshare_unserialized_data',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];

  $data['ayrshare_analytics']['platform'] = [
    'title' => t('Platform'),
    'help' => t('Social platform name.'),
    'field' => [
      'id' => 'standard',
    ],
    'sort' => [
      'id' => 'standard',
    ],
    'filter' => [
      'id' => 'string',
    ],
  ];

  return $data;
}
