<?php

namespace Drupal\ayrshare_node\Database;

use Drupal\Core\Database\Connection;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements Ayrshare database storage class.
 */
class DatabaseStorage implements ContainerInjectionInterface {

  /**
   * The database connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs a new DatabaseStorage object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection object.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function find($table, $nid, $top_level = TRUE, $platform = NULL) {
    try {
      $select = $this->database->select("ayrshare_$table", 'at', ['fetch' => \PDO::FETCH_ASSOC]);
      $select->condition('nid', $nid);

      if ($top_level) {
        $select->fields('at', ['tid']);
      }
      else {
        $select->fields('at', ['pid']);
      }

      if ($platform) {
        $select->condition('platform', $platform);
      }

      $select->orderBy('id', 'DESC');
      $select->range(0, 1);
    }
    catch (\Exception $e) {
      // Log error.
    }

    return $select->execute()->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  private function exists($table, array $unique_field): bool {
    try {
      $select = $this->database->select("ayrshare_$table", 'at', ['fetch' => \PDO::FETCH_ASSOC]);
      $select->condition($unique_field['name'], $unique_field['value']);
      $select->fields('at', [$unique_field['name']]);
      $exists = !empty($select->execute()->fetchField());
    }
    catch (\Exception $e) {
      // Log error.
    }

    return $exists;
  }

  /**
   * {@inheritdoc}
   */
  public function insert($table, $fields, $unique_field = FALSE) {
    try {
      if ($unique_field && $this->exists($table, $unique_field)) {
        return;
      }

      $this->database->insert("ayrshare_$table")->fields($fields)->execute();
    }
    catch (\Exception $e) {
      // Log error.
    }
  }

  /**
   * {@inheritdoc}
   */
  public function delete($table, $fields = []) {
    try {
      $query = $this->database->delete("ayrshare_$table");

      foreach ($fields as $key => $value) {
        $query->condition($key, $value);
      }

      $query->execute();
    }
    catch (\Exception $e) {
      // Log error.
    }
  }

  /**
   * {@inheritdoc}
   */
  public function purge($table) {
    try {
      $this->database->truncate("ayrshare_$table")->execute();
    }
    catch (\Exception $e) {
      // Log error.
    }
  }

}
