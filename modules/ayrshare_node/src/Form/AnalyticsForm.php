<?php

namespace Drupal\ayrshare_node\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for Ayrshare API Analytics action.
 */
class AnalyticsForm extends AyrshareBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ayrshare_node_analytics_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node = NULL) {
    $form = parent::buildForm($form, $form_state);
    $form['actions']['submit']['#value'] = $this->t('Get Analytics');
    $form['nid']['#value'] = $node;
    $platforms_options = $form['platforms']['#options'];

    // See https://docs.ayrshare.com/rest-api/endpoints/analytics#analytics-on-a-post.
    $form['platforms']['#options'] = array_intersect_key($platforms_options, [
      'facebook' => 'Facebook',
      'instagram' => 'Instagram',
      'twitter' => 'Twitter',
      'tiktok' => 'Tiktok',
      'pinterest' => 'Pinterest',
    ]);

    if ($analytics_data = $form_state->get('analytics_data')) {
      $form['analytics_data'] = [
        '#markup' => "<pre>$analytics_data</pre>",
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($nid = $form_state->getValue('nid')) {
      $platforms = array_values($form_state->getValue('platforms'));
      $platforms = array_filter($platforms);
      $platforms_analytics_data = [];

      foreach ($platforms as $platform) {
        $tid = $this->databaseStorage->find('post', $nid, TRUE, $platform);

        if ($tid) {
          $platform_analytics_data = $this->apiClient->analyticsPost($tid, [$platform]);
          $platforms_analytics_data[] = $platform_analytics_data;

          if (!empty($platform_analytics_data)) {
            $tid = $platform_analytics_data['id'];

            foreach ($platform_analytics_data as $platform_data) {
              if (is_array($platform_data)) {
                $fields = [
                  'tid' => $tid,
                  'pid' => $platform_data['id'],
                  'nid' => $nid,
                  'data' => serialize($platform_data['analytics']),
                  'platform' => $platform,
                ];

                $this->databaseStorage->insert('analytics', $fields, [
                  'name' => 'pid',
                  'value' => $platform_data['id'],
                ]);
              }
            }
          }
        }
      }

      $analytics_data = json_encode($platforms_analytics_data,
        JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE
      );
      $form_state->set('analytics_data', $analytics_data);
      $form_state->setRebuild();
    }
  }

}
