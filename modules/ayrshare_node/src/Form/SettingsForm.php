<?php

namespace Drupal\ayrshare_node\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements the module settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type bundle info manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $bundle_info
   *   The bundle information service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeBundleInfoInterface $bundle_info) {
    parent::__construct($config_factory);
    $this->entityTypeBundleInfo = $bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.bundle.info'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ayrshare_node.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ayrshare_node_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ayrshare_node.settings');

    $node_bundles = $this->entityTypeBundleInfo->getBundleInfo('node');

    $form['intro'] = [
      '#type' => 'markup',
      '#markup' => $this->t('Enable Ayrshare for the selected content types.'),
    ];

    foreach ($node_bundles as $name => $label) {
      $form[$name . '_enable'] = [
        '#type' => 'checkbox',
        '#title' => reset($label),
        '#default_value' => $config->get($name . '_enable'),
      ];

      // Only show this field when the checkbox for the bundle is enabled.
      $form[$name . '_fields'] = [
        '#type' => 'textarea',
        '#title' => t('Social Platform - Drupal fields mapping'),
        '#default_value' => $config->get($name . '_fields'),
        '#description' => $this->t('Enter one value per line, in the format
        "platform_name|field_name|value", eg: facebook|body|summary.'),
        '#placeholder' => $this->t("facebook|body|summary\ntwitter|field_twitter|value\netc"),
        '#rows' => 10,
        '#states' => [
          'visible' => [
            ':input[name="' . $name . '_enable"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $edit_config = $this->configFactory->getEditable('ayrshare_node.settings');

    foreach ($values as $key => $value) {
      $edit_config->set($key, $value);
    }

    $edit_config->save();

    parent::submitForm($form, $form_state);
  }

}
