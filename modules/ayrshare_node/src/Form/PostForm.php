<?php

namespace Drupal\ayrshare_node\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for Ayrshare API Post action.
 */
class PostForm extends AyrshareBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ayrshare_node_post_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node = NULL) {
    $form = parent::buildForm($form, $form_state);
    $form['actions']['submit']['#value'] = $this->t('Post');
    $form['nid']['#value'] = $node;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($nid = $form_state->getValue('nid')) {

      $node = $this->entityTypeManager->getStorage('node')->load($nid);
      $config_name = $node->bundle() . '_fields';
      $node_config = $this->config('ayrshare_node.settings')->get($config_name);
      $platform_fields = $this->getPlatformFields($node_config);
      $platforms = array_values($form_state->getValue('platforms'));
      $platforms = array_filter($platforms);

      foreach ($platforms as $platform) {
        if (!array_key_exists($platform, $platform_fields)) {
          continue;
        }

        $field_name = $platform_fields[$platform]['field'];

        if (!$node->hasField($field_name)) {
          continue;
        }

        $field_value = $node->get($field_name)->{$platform_fields[$platform]['value']};
        $post_ids = $this->apiClient->post($field_value, [$platform]);

        if (!empty($post_ids)) {
          $tid = key($post_ids);
          $post_ids = reset($post_ids);

          foreach ($post_ids as $pid) {
            $fields = [
              'tid' => $tid,
              'pid' => $pid,
              'nid' => $nid,
              'platform' => $platform,
            ];

            $this->databaseStorage->insert('post', $fields, [
              'name' => 'pid',
              'value' => $pid,
            ]);
          }
        }
      }
    }
  }

  /**
   * Helper to get the node fields from the module configuration.
   *
   * @param string $platform_fields
   *   Contains the map between platforms and source fields.
   *
   * @return array
   *   Array containing social_platform => field_name mappings.
   */
  private function getPlatformFields($platform_fields) {
    $result = [];

    // Break string to lines.
    $lines = explode(PHP_EOL, $platform_fields);

    // Get mappings from every line.
    foreach ($lines as $line) {
      $mapping = explode('|', $line);
      // Only if all parts are available.
      if (count($mapping) === 3) {
        // Add mappings to result.
        $result[trim($mapping[0])]['field'] = trim($mapping[1]);
        $result[trim($mapping[0])]['value'] = trim($mapping[2]);
      }
    }

    return $result;
  }

}
