<?php

namespace Drupal\ayrshare_node\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for Ayrshare API Comments action.
 */
class CommentsForm extends AyrshareBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ayrshare_node_comments_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node = NULL) {
    $form = parent::buildForm($form, $form_state);
    $form['actions']['post'] = [
      '#type' => 'submit',
      '#value' => $this->t('Post Comment'),
      '#submit' => ['::postComment'],
      '#button_type' => 'primary',
    ];

    if ($comments_data = $form_state->get('platforms_comments_data')) {
      $form['comments_data'] = [
        '#markup' => "<pre>$comments_data</pre>",
      ];
    }

    $form['comment'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Comment message'),
      '#rows' => 5,
    ];

    $form['actions']['submit']['#value'] = $this->t('Get Comments');
    $form['nid']['#value'] = $node;
    $platforms_options = $form['platforms']['#options'];

    // See https://docs.ayrshare.com/rest-api/endpoints/comments#post-a-comment.
    $form['platforms']['#options'] = array_intersect_key($platforms_options, [
      'facebook' => 'Facebook',
      'instagram' => 'Instagram',
    ]);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($nid = $form_state->getValue('nid')) {
      $platforms = array_values($form_state->getValue('platforms'));
      $platforms = array_filter($platforms);
      $platforms_comments_data = '';

      foreach ($platforms as $platform) {
        $tid = $this->databaseStorage->find('post', $nid, TRUE, $platform);

        if ($tid) {
          $platforms_comments = $this->apiClient->getComments($tid);
          $this->saveComments($nid, $platforms_comments);

          $platforms_comments_data = json_encode($platforms_comments,
            JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE
          );
        }
      }

      $form_state->set('platforms_comments_data', $platforms_comments_data);
      $form_state->setRebuild();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postComment(array &$form, FormStateInterface $form_state) {
    if ($nid = $form_state->getValue('nid')) {
      if ($comment = $form_state->getValue('comment')) {
        $platforms = array_values($form_state->getValue('platforms'));
        $platforms = array_filter($platforms);
        $platforms_comments = [];

        foreach ($platforms as $platform) {
          $tid = $this->databaseStorage->find('post', $nid, TRUE, $platform);

          if ($tid) {
            $this->apiClient->postComments($tid, [$platform], $comment);
            $platform_comment = $this->apiClient->getComments($tid);
            $this->saveComments($nid, $platform_comment);
            $platforms_comments[] = $platform_comment;
          }
        }

        $platforms_comments_data = json_encode($platforms_comments,
          JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE
        );
        $form_state->set('platforms_comments_data', $platforms_comments_data);
        $form_state->setRebuild();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  private function saveComments($nid, $platforms_comments) {
    if (!empty($platforms_comments)) {
      $tid = $platforms_comments['id'];

      foreach ($platforms_comments as $platform_comments) {
        if (is_array($platform_comments)) {
          foreach ($platform_comments as $platform_comment) {
            $fields = [
              'tid' => $tid,
              'cid' => $platform_comment['commentId'],
              'nid' => $nid,
              'comment' => $platform_comment['comment'],
              'created' => $platform_comment['created'],
              'platform' => $platform_comment['platform'],
            ];

            if (array_key_exists('from', $platform_comment)) {
              $fields['from'] = serialize($platform_comment['from']);
            }

            $this->databaseStorage->insert('comment', $fields, [
              'name' => 'cid',
              'value' => $platform_comment['commentId'],
            ]);
          }
        }

      }
    }
  }

}
