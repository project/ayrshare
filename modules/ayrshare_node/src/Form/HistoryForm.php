<?php

namespace Drupal\ayrshare_node\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for Ayrshare API History action.
 */
class HistoryForm extends AyrshareBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ayrshare_node_history_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node = NULL) {
    $form = parent::buildForm($form, $form_state);
    $form['actions']['submit']['#value'] = $this->t('Get History');
    $form['nid']['#value'] = $node;

    if ($history = $form_state->get('history')) {
      $form['history'] = [
        '#markup' => "<pre>$history</pre>",
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($nid = $form_state->getValue('nid')) {
      $platforms = array_values($form_state->getValue('platforms'));
      $platforms = array_filter($platforms);
      $histories = [];

      foreach ($platforms as $platform) {
        $tid = $this->databaseStorage->find('post', $nid, TRUE, $platform);

        if ($tid) {
          $history = $this->apiClient->historyById($tid);
          $histories[] = $history;
        }
      }

      $history = json_encode($histories,
        JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE
      );
      $form_state->set('history', $history);
      $form_state->setRebuild();
    }
  }

}
