<?php

namespace Drupal\ayrshare_node\Form;

use Drupal\ayrshare\AyrshareApiClient;
use Drupal\ayrshare_node\Database\DatabaseStorage;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for Ayrshare API Post action.
 */
class AyrshareBaseForm extends FormBase {

  /**
   * The Ayrshare API client.
   *
   * @var \Drupal\ayrshare\AyrshareApiClient
   */
  protected $apiClient;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The post storage.
   *
   * @var \Drupal\ayrshare_node\Database\DatabaseStorage
   */
  protected $databaseStorage;

  /**
   * Constructs a new AyrshareBaseForm object.
   *
   * @param \Drupal\ayrshare\AyrshareApiClient $api_client
   *   The Ayrshare API client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\ayrshare_node\Database\DatabaseStorage $post_storage
   *   The post storage service.
   */
  public function __construct(AyrshareApiClient $api_client, EntityTypeManagerInterface $entity_type_manager, DatabaseStorage $post_storage) {
    $this->apiClient = $api_client;
    $this->entityTypeManager = $entity_type_manager;
    $this->databaseStorage = $post_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ayrshare.api_client'),
      $container->get('entity_type.manager'),
      $container->get('ayrshare_node.database_storage'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node = NULL) {
    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#submit' => ['::submitForm'],
      '#button_type' => 'primary',
    ];

    $form['nid'] = [
      '#type' => 'hidden',
      '#value' => $node,
    ];

    if ($user = $this->apiClient->user()) {
      $platforms = $user['displayNames'];
      foreach ($platforms as $platform) {
        $name = $platform['platform'];
        $target_platforms[$name] = $platform['displayName'] . ' (' . $name . ')';
      }
      $form['platforms'] = [
        '#type' => 'checkboxes',
        '#required' => TRUE,
        '#title' => $this->t('Target Platforms'),
        '#options' => $target_platforms,
        '#description' => $this->t('Please select the desired target social accounts.'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ayrshare_node_base_form';
  }

}
