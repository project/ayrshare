<?php

namespace Drupal\ayrshare_node\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for Ayrshare API Delete action.
 */
class DeleteForm extends AyrshareBaseForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ayrshare_node_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node = NULL) {
    $form = parent::buildForm($form, $form_state);
    $form['delete_confirm'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Deletion confirmation'),
      '#description' => $this->t('If you want to proceed please write DELETE.'),
      '#required' => TRUE,
    ];

    $form['actions']['submit']['#value'] = $this->t('Delete');
    $form['nid']['#value'] = $node;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($nid = $form_state->getValue('nid')) {
      $platforms = array_values($form_state->getValue('platforms'));
      $platforms = array_filter($platforms);

      if ($form_state->getValue('delete_confirm') == 'DELETE') {
        foreach ($platforms as $platform) {
          $tid = $this->databaseStorage->find('post', $nid, TRUE, $platform);

          if ($tid) {
            $this->apiClient->postDelete($tid);
            $this->databaseStorage->delete('post', [
              'nid' => $nid,
              'tid' => $tid,
            ]);
          }
        }
      }
    }
  }

}
