<?php

namespace Drupal\ayrshare_node\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Checks access for displaying Ayrshare social tools pages.
 */
class AccessCheck implements AccessInterface {

  /**
   * The ayrshare node configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->config = $config_factory->get('ayrshare_node.settings');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Checks user access to use Ayrshare on a given node bundle.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match to check against.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(RouteMatchInterface $route_match) {
    if ($nid = $route_match->getParameter('node')) {
      if ($node = $this->entityTypeManager->getStorage('node')->load($nid)) {
        $config_name = $node->bundle() . '_enable';
        $ayshare_enabled = $this->config->get($config_name);

        if ($ayshare_enabled) {
          return AccessResult::allowed();
        }
      }
    }
    return AccessResult::forbidden();
  }

}
