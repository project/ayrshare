<?php

namespace Drupal\ayrshare_node\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to present serialized data in Json format.
 *
 * @group views_field_handlers
 *
 * @ViewsField("ayrshare_unserialized_data")
 */
class UnserializedData extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $value = unserialize($this->getValue($values));
    return json_encode($value,
      JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE
    );
  }

}
