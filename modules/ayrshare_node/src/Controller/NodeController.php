<?php

namespace Drupal\ayrshare_node\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides an index page with help information and CTA links.
 *
 * @package Drupal\ayrshare_node\Controller
 */
class NodeController extends ControllerBase {

  /**
   * Social Tools index page containing Ayrshare information and CTA links.
   *
   * @return string
   *   Return Hello string.
   */
  public function tools($node) {
    return [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => t("Ayrshares gives you the tools to schedule social 
        media posts to Twitter, Facebook Pages and Groups, LinkedIn, Instagram, 
        YouTube, Reddit, Google My Business, Pinterest, TikTok, and Telegram via 
        a REST API. You can post to all your, or your user's, linked social 
        networks with a single API call."),
    ];
  }

}
