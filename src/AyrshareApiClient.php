<?php

namespace Drupal\ayrshare;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Ayrshare API client wrapper class.
 */
class AyrshareApiClient {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * Ayrshare API base url.
   *
   * @var string
   */
  protected $apiBaseUrl;

  /**
   * Ayrshare API key.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new AyrshareApiClient object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The UUID service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    ClientInterface $http_client,
    ConfigFactoryInterface $config_factory,
    UuidInterface $uuid_service,
    MessengerInterface $messenger,
    LoggerChannelFactoryInterface $logger) {
    $this->httpClient = $http_client;
    $this->uuidService = $uuid_service;
    $config = $config_factory->get('ayrshare.settings');
    $this->apiBaseUrl = $config->get('api_url');
    $this->apiKey = $config->get('api_key');
    $this->messenger = $messenger;
    $this->logger = $logger->get('ayrshare');
  }

  /**
   * Performs an Ayrshare API request. Wraps the Guzzle HTTP client.
   *
   * @param string $method
   *   HTTP method.
   * @param string $endpoint
   *   The API endpoint to call.
   * @param array $body
   *   The body to send to the API endpoint.
   * @param array $query
   *   API call query parameters.
   * @param string $content_type
   *   The "Content-Type" header value.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   The request response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @see \GuzzleHttp\ClientInterface::request
   */
  public function request($method,
    $endpoint,
    array $body = [],
    array $query = [],
    $content_type = 'application/json') {

    $endpoint = $this->apiBaseUrl . $endpoint;

    if ($method === 'GET') {
      $endpoint = Url::fromUri($endpoint, [
        'query' => $query,
      ])->toString();
    }

    $request = [
      'headers' => [
        'Authorization' => 'Bearer ' . $this->apiKey,
        'Content-Type' => $content_type,
      ],
      'timeout' => 10,
    ];

    if ($body) {
      $request['body'] = Json::encode($body);
    }

    try {
      $response = $this->httpClient->request($method, $endpoint, $request);

      if ($response->getStatusCode() != 200) {
        $this->messenger->addError(t('@error', [
          '@error' => $response->getReasonPhrase(),
        ]));
        $this->logger->error('Request failed: @response.', [
          '@response' => $response->getReasonPhrase(),
        ]);
      }

      $this->logger->notice('Response from %endpoint: 200 @response.', [
        '%endpoint' => $endpoint,
        '@response' => $response->getReasonPhrase(),
      ]);
    }
    catch (Exception $e) {
      $response = $e->getResponse();

      $this->logger->error('Response from %endpoint: @status_code @response.', [
        '%endpoint' => $endpoint,
        '@response' => $e->getResponse(),
        '@status_code' => $e->getResponse()->getStatusCode(),
      ]);
    }

    return $this->parseReponse($response);
  }

  /**
   * Parses the nsw_trustee API JSON response.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   API response object.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function parseReponse(ResponseInterface $response) {
    $data = $response->getBody()->getContents();
    $data = Json::decode($data);
    return $data;
  }

  /**
   * Redirect to the given url with given redirect code.
   *
   * @param string $url
   *   The URL to redirect to.
   * @param string $code
   *   The redirect code response.
   */
  public function redirectResponse($url, $code) {
    $response = new RedirectResponse($url, $code);
    $response->send();
  }

  /**
   * Analytics on a Shortened Link.
   *
   * @param int $lastdays
   *   History of links posted over the past n days. Range 1-7 days.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function analyticsLinks($lastdays = 1) {
    $response = $this->request('GET', '/analytics/links', [], [
      'lastDays' => $lastdays,
    ]);

    return $response;
  }

  /**
   * Get real-time analytics for a given post ID.
   *
   * Will only return the analytics of networks where the post was sent.
   *
   * @param string $id
   *   Post ID returned from the /post endpoint. This is the top level "id"
   *   returned and not the ids in "postIds".
   * @param array $platforms
   *   String array of platforms to retrieve analytics.
   *   For example: "instagram", "facebook", "twitter", "linkedin",
   *   "youtube", "pinterest"].
   * @param string $profile_key
   *   Get analytics for a particular user profile by passing the profileKey.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function analyticsPost($id, array $platforms, $profile_key = NULL) {
    $data = [
      'id' => $id,
      'platforms' => $platforms,
    ];

    if ($profile_key) {
      $data['profileKey'] = $profile_key;
    }

    $response = $this->request('POST', '/analytics/post', $data);

    return $response;
  }

  /**
   * Get analytics, such as followers or username, at the social network level.
   *
   * @param array $platforms
   *   Social media platforms to get analytics. Accepts an array of Strings with
   *   values: "facebook", "instagram", "twitter", "tiktok", "pinterest".
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function analyticsSocial(array $platforms) {
    $response = $this->request('POST', '/analytics/social', [
      'platforms' => $platforms,
    ]);

    return $response;
  }

  /**
   * Automatically add hashtags to your post. Maximum post length is 1,000.
   *
   * If over limit, hashtags will not be added.
   *
   * @param string $post
   *   Post text to add the hashtags. Max length 1,000 characters.
   * @param string $position
   *   Values: "auto" or "end". "end" hashtags at the end of the text, "auto"
   *   hashtags anywhere in the text or at the end. Default "auto".
   * @param int $max
   *   Integer of max number of Hashtags to add. Max value range 1 to 5.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function autoHashtag($post, $position = 'auto', $max = 2) {
    $response = $this->request('POST', '/auto-hashtag', [
      'post' => $post,
      'max' => $max,
      'position' => $position,
    ]);

    return $response;
  }

  /**
   * Set up an auto-post schedule by providing times to send.
   *
   * Post will automatically be sent at the next available time.
   *
   * @param array $schedule
   *   Array of strings of scheduled times to auto-posts.
   *   Format: ISO-8601 UTC. Example: ["13:05Z", "22:14Z"].
   *   Not required if setStartDate provided.
   * @param string $title
   *   Multiple schedules can be set by providing a title for each schedule.
   *   If you specify the title in /post with autoScheduleTitle, that schedule
   *   will be used.
   *   Default title is: "default".
   * @param string $set_start_date
   *   Set a specific beginning date to start the auto schedule, provide a
   *   ISO-8601 UTC date time. E.g. 2021-07-08T12:30:00Z. The start time will
   *   be applied to the provided "title" or will use the default title of one
   *   isn't provided.
   * @param string $profile_key
   *   Auto-post for a particular user profile by passing the profileKey.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function autoSchedule(array $schedule,
    $title = NULL,
    $set_start_date = NULL,
    $profile_key = NULL) {
    $data = [
      'title' => $title,
    ];

    if ($schedule) {
      $data['schedule'] = $schedule;
    }

    if ($set_start_date) {
      $data['setStartDate'] = $set_start_date;
    }

    if ($profile_key) {
      $data['profileKey'] = $profile_key;
    }

    $response = $this->request('POST', '/auto-schedule/set', $data);

    return $response;
  }

  /**
   * Delete a particular auto schedule. Provide the title of the schedule.
   *
   * @param string $title
   *   Schedule title previously set in /auto-schedule/set endpoint.
   *   If a title is not given, "default" title is used.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function autoScheduleDelete($title) {
    $response = $this->request('DELETE', '/auto-schedule/delete', [
      'title' => $title,
    ]);

    return $response;
  }

  /**
   * Returns an array of schedules with titles, times, and last scheduled date.
   *
   * The next auto scheduled post will start at the last scheduled date.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function autoScheduleList() {
    $response = $this->request('GET', '/auto-schedule/list');

    return $response;
  }

  /**
   * Add a comment to a post. Currently only on Facebook Pages and Instagram.
   *
   * @param string $id
   *   Post top-level ID.
   * @param array $platforms
   *   List of platforms to add comments.
   *   Currently available platforms: ["facebook", "instagram"].
   * @param string $comment
   *   Text of the new comment to add to the post.
   * @param string $profile_key
   *   Post comments for a particular user profile by passing the profileKey.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function postComments($id, array $platforms, $comment, $profile_key = NULL) {
    $body = [
      'id' => $id,
      'platforms' => $platforms,
      'comment' => $comment,
    ];

    if ($profile_key) {
      $body['profileKey'] = $profile_key;
    }

    $response = $this->request('POST', '/comments', $body);

    return $response;
  }

  /**
   * Get the comments for a given top-level post ID.
   *
   * Currently only Facebook Pages and Instagram.
   *
   * @param string $id
   *   Post top-level ID.
   * @param string $profile_key
   *   Get comments for a particular user profile by passing the profileKey.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function getComments($id, $profile_key = NULL) {
    $query = [
      'id' => $id,
    ];

    if ($profile_key) {
      $query['profileKey'] = $profile_key;
    }

    $response = $this->request('GET', '/comments', [], $query);

    return $response;
  }

  /**
   * Add a new RSS feed for automated posting of new articles.
   *
   * @param string $url
   *   URL of the RSS feed to add.
   * @param array $options
   *   Body optional argumens: useFirstImage, autoHashtag and type.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function feed($url, array $options = []) {
    $options['url'] = $url;
    $response = $this->request('POST', '/feed', $options);

    return $response;
  }

  /**
   * Delete an RSS Feed that was previously added.
   *
   * @param string $id
   *   ID of the RSS Feed returned when adding.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function feedDelete($id) {
    $response = $this->request('DELETE', '/feed', [
      'id' => $id,
    ]);

    return $response;
  }

  /**
   * Webhook that will be called whenever a new RSS items is available.
   *
   * @param string $url
   *   URL of your webhook to call when a new RSS items is available.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function feedWebhook($url) {
    $response = $this->request('POST', '/feed/webhook', [
      'url' => $url,
    ]);

    return $response;
  }

  /**
   * Get a history of all posts sent via Ayrshare, in descending order.
   *
   * This includes future scheduled posts with a status of "pending".
   *
   * @param array $query
   *   Optional query arguments: status, lastDays, lastRecords.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function history(array $query) {
    $response = $this->request('GET', '/history', [], $query);

    return $response;
  }

  /**
   * Get the history for a specific post.
   *
   * @param string $id
   *   Ayrshare top level post ID.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function historyById($id) {
    $response = $this->request('GET', '/history/' . $id);

    return $response;
  }

  /**
   * Get all the past Instagram, Facebook, or TikTok posts and analytics.
   *
   * Returns up to 100 posts.
   *
   * @param string $platfom
   *   Path argument: instagram, facebook, tiktok.
   * @param int $limit
   *   Number of history records to return. Default 10, max 100.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function historyByPlatform($platfom, $limit = 10) {
    $response = $this->request('GET', '/history/' . $platfom, [], [
      'limit' => $limit,
    ]);

    return $response;
  }

  /**
   * This endpoint allows you to upload a file or an image or video.
   *
   * Returned will be the URL to the image that can be used in a post.
   *
   * @param string|object $file
   *   Send the media file as a Base64 encoded string as a Data URI string.
   *   The string should begin with 'data:content/type;base64'.
   * @param string $file_name
   *   The name of the file for later reference.
   * @param string $description
   *   A description for later reference.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function mediaUpload($file, $file_name = NULL, $description = NULL) {
    $body = [
      'file' => $file,
    ];

    if ($file_name) {
      $body['fileName'] = $file_name;
    }

    if ($description) {
      $body['description'] = $description;
    }

    $response = $this->request('POST', '/media/upload', $body, [],
    'multipart/form-data');

    return $response;
  }

  /**
   * For file uploads greater than 10 MB, obtain a URL to upload a file.
   *
   * @param string $file_name
   *   Name of the file to be uploaded. Must include an extension such as
   *   .png, .jpg, .mov, .mp4, etc.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function mediaUploadUrl($file_name) {
    $query = [
      'fileName' => $file_name,
    ];

    $response = $this->request('GET', '/media/uploadUrl', [], $query);

    return $response;
  }

  /**
   * Retrieve all the images uploaded to the image gallery.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function media() {
    $response = $this->request('GET', '/media');

    return $response;
  }

  /**
   * Post to the social networks you have enabled in the web dashboard.
   *
   * If you want to post to one or many client profiles, use the /profiles.
   *
   * @param string $post
   *   The post text sent to the social networks specified in the platforms
   *   parameter. You can include URLs in the post or rich text.
   * @param array $platforms
   *   Social media platforms to post. Accepts an array of Strings with values:
   *   "facebook", "fbg", "twitter", "linkedin", "instagram", "youtube",
   *   "reddit", "telegram", "gmb", "pinterest", or "tiktok".
   * @param array $options
   *   Optional body parameters: mediaUrls, isVideo, scheduleDate, shortenLinks,
   *   autoSchedule, autoRepost, autoHashTag, unsplash, faceBookOptions,
   *   GmbOptions, instagramOptions, pinterestOptions, redditOptions,
   *   youtubeOptions, ads.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function post($post, array $platforms, array $options = []) {
    $options['post'] = $post;
    $options['platforms'] = $platforms;
    $post_ids = [];
    $response = $this->request('POST', '/post', $options);

    if ($response) {
      $post_ids[$response['id']] = [];
      foreach ($response['postIds'] as $post) {
        $post_ids[$response['id']][] = $post['id'];

        $this->messenger->addMessage(t('🥳🥳 View the :platform publication 
        <a href=":url" target="_blank">here.</a>', [
          ':platform' => ucfirst($post['platform']),
          ':url' => $post['postUrl'],
        ]));
      }

      foreach ($response['errors'] as $error) {
        $this->messenger->addError(t('@error', [
          '@error' => $error,
        ]));
      }
    }

    return $post_ids;
  }

  /**
   * Deletes a post from all social networks or bulk delete scheduled posts.
   *
   * Takes a post id returned from the /post.
   *
   * @param string $id
   *   Id of the post to delete. Not required if bulk or deleteAllScheduled
   *   parameter sent.
   * @param bool $delete_all_scheduled
   *   If true will delete all scheduled posts still in a pending state - has
   *   not yet been published to the networks.
   *   Required if id or bulk parameter not sent.
   * @param array $bulk
   *   Array of Strings post Ids to bulk delete.
   *   Required if id or deleteAllScheduled parameter not sent.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function postDelete($id = NULL, $delete_all_scheduled = NULL, array $bulk = []) {
    $body = [];

    if ($id) {
      $body['id'] = $id;
    }

    if ($delete_all_scheduled) {
      $body['deleteAllScheduled'] = $delete_all_scheduled;
    }

    if ($bulk) {
      $body['bulk'] = $bulk;
    }

    $response = $this->request('DELETE', '/post', $body);

    return $response;
  }

  /**
   * Bulk schedule posts with CSV (Comma Separated Values) file of posts data.
   *
   * @param object $file
   *   Multipart form-data CSV file of scheduled posts.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function postBulk($file) {
    $response = $this->request('POST', '/post/bulk', [
      'file' => $file,
    ], [], 'multipart/form-data');

    return $response;
  }

  /**
   * Calculate the weighted length, or character count, of a post string.
   *
   * @param string $post
   *   Post string to calculated weighted length.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function checkPostWeight($post) {
    $response = $this->request('POST', '/post/checkPostWeight', [
      'post' => $post,
    ]);

    return $response;
  }

  /**
   * Create a new profile under your primary account.
   *
   * The Profile Key of the newly created profile is returned.
   *
   * Use the Profile Key to manage and post on behalf of your client.
   *
   * @param string $title
   *   Title of the new profile. Must be unique.
   * @param array $options
   *   Optional hideTitle, displayTitle and disableSocial options.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function createProfile($title, array $options = []) {
    $options['title'] = $title;

    $response = $this->request('POST', '/profiles/create-profile', $options);

    return $response;
  }

  /**
   * Delete a profile you are the owner of.
   *
   * @param string $profile_key
   *   The profileKey of the profile to delete, returned from /create-profile.
   *   May also use the API Key found in the web dashboard.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function deleteProfile($profile_key) {
    $response = $this->request('DELETE', '/profiles/create-profile', [
      'profileKey' => $profile_key,
    ]);

    return $response;
  }

  /**
   * Update an existing profile's title, hide title, display title, etc.
   *
   * @param string $profile_key
   *   The Profile key value.
   * @param array $options
   *   Optional title, tikTokId, hideTitle, displayTitle and disableSocial
   *   options.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function updateProfile($profile_key, array $options = []) {
    $options['profileKey'] = $profile_key;

    $response = $this->request('PUT', '/profiles/update-profile', $options);

    return $response;
  }

  /**
   * Get all the profiles associated with the primary account.
   *
   * @param string $title
   *   Return only the profile associated with the  URL encoded title.
   * @param string $ref_id
   *   Return only the profile associated with the given refId.
   *   The refId was returned during the profile creation or from the /user
   *   endpoint.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function profiles($title = NULL, $ref_id = NULL) {
    $query = [];

    if ($title) {
      $query['title'] = $title;
    }

    if ($ref_id) {
      $query['refId'] = $ref_id;
    }

    $response = $this->request('GET', '/profiles', [], $query);

    return $response;
  }

  /**
   * Generate a JSON Web Token for use with single sign on.
   *
   * Used if you do not want to generate the JWT yourself.
   *
   * Also provided is the URL that can directly be used for SSO.
   *
   * The JWT URL is valid for 5 minutes.
   *
   * @param string $domain
   *   Domain of app. Please use the exact domain given during onboarding.
   * @param string $private_key
   *   Private Key use for encryption.
   * @param string $profile_key
   *   User Profile key.
   * @param array $options
   *   Optional logout and redirect body parameters.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function profilesGenerateJwt(
  $domain,
  $private_key,
  $profile_key,
  array $options = []) {
    $options['domain'] = $domain;
    $options['private_Key'] = $private_key;
    $options['profileKey'] = $profile_key;

    $response = $this->request('POST', '/profiles/generateJWT', $options);

    return $response;
  }

  /**
   * Short API Endpoint: Shorten links using Ayrshare's URL shortener.
   *
   * @param string $url
   *   URL to be shortened. Must be a valid URL starting with "https://".
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function short($url) {
    $response = $this->request('POST', '/shorten', [
      'url' => $url,
    ]);

    return $response;
  }

  /**
   * Get information on the user or user profile.
   *
   * @param bool $instagram_quota
   *   Return the current Instagram quota used - 25 posts over a 24-hour
   *   rolling period.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function user($instagram_quota = FALSE) {
    $query = [];

    if ($instagram_quota) {
      $query['instagramQuota'] = $instagram_quota;
    }

    $response = $this->request('GET', '/user', [], $query);

    return $response;
  }

  /**
   * Get the details of a particular platform. Current support for Pinterest.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function userDetails($platform = 'pinterest') {
    $response = $this->request('GET', '/user/details/' . $platform);

    return $response;
  }

  /**
   * Register a webhook to receive asynchronous updates on events.
   *
   * @param string $method
   *   POST, DELETE, GET.
   * @param string $action
   *   Available actions: feed, social, scheduled.
   * @param string $url
   *   The URL to be called on action. URL must be in a valid format and
   *   begin with "https://".
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function webhooks($method, $action, $url) {
    $data = [];

    if ($action) {
      $data['action'] = $action;
    }

    if ($url) {
      $data['url'] = $url;
    }

    $response = $this->request($method, '/hook/webhook', $data);

    return $response;
  }

  /**
   * The list of Ad campaigns associated with your Twitter account.
   *
   * The campaigns field id contains the campaign ID.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function adsTwitterCampaigns() {
    $response = $this->request('GET', '/ads/twitter/campaigns');

    return $response;
  }

  /**
   * The Ad Groups are the groupings under a given Campaign.
   *
   * The id of an element is the key information need for the promote Tweet.
   *
   * @param string $id
   *   Campaign ID.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function adsTwitterGroups($id) {
    $response = $this->request('GET', '/ads/twitter/groups/' . $id);

    return $response;
  }

  /**
   * Set the "campaign" and "group" ids for a given profile.
   *
   * Used in /post for automatic promoting.
   *
   * @param string $campaign
   *   Campaign ID for use in /post.
   * @param string $group
   *   Group ID for use in /group.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function adsTwitterIds($campaign, $group) {
    $response = $this->request('PUT', '/ads/twitter/ids', [
      'campaign' => $campaign,
      'group' => $group,
    ]);

    return $response;
  }

  /**
   * Promote Tweets by passing in a Campaign ID, Group ID, and Tweet IDs.
   *
   * @param array $ads
   *   Array of Tweets to promote.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function ads(array $ads) {
    $response = $this->request('POST', '/ads', $ads);

    return $response;
  }

  /**
   * Deletes Tweets by passing in a Campaign ID, Group ID, and Tweet IDs.
   *
   * @param array $ads
   *   Array of Tweets to delete.
   *
   * @return array
   *   Json from API response parsed to array format.
   */
  public function adsDelete(array $ads) {
    $response = $this->request('DELETE', '/ads', $ads);

    return $response;
  }

}
