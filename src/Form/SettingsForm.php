<?php

namespace Drupal\ayrshare\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the module settings form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ayrshare.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ayrshare_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ayrshare.settings');

    $form['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API base url'),
      '#description' => $this->t('API base url: https://app.ayrshare.com/api'),
      '#default_value' => $config->get('api_url'),
    ];

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#description' => $this->t('Put here your Ayrshare API key. Get it from https://app.ayrshare.com/api.'),
      '#default_value' => $config->get('api_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configFactory->getEditable('ayrshare.settings')
      ->set('api_key', $values['api_key'])
      ->set('api_url', $values['api_url'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
